# React Base Project

# Required
* NodeJS
* yarn

# Make env file
You should create `.env` file.

If you have stating and production env, you should create `.env.staging` and `.env.production` also.

# How to start
`yarn start`

# How to build
`yarn build:production`
