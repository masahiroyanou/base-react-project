import moment from 'moment';

export class User {

  static parse(obj) {
    const user = new User();
    user.id = obj.id ? obj.id : '';
    user.name = obj.name ? obj.name : '';
    user.createdTime = obj.createdTime ? moment(obj.createdTime) : moment();

    return user;
  }
}
