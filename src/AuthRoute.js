import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import App from './containers/App';
import { skipLogin } from './actions/auth';

class AuthRoute extends Component {

  componentDidMount() {
    const { isAuthenticated, location, dispatch } = this.props;
    if (isAuthenticated) {
      return;
    }

    const token = localStorage.getItem('token');
    if (token && token.length) {
      dispatch(skipLogin(token, location.pathname + location.search));
    }
  }

  render() {
    const { isAuthenticated, ...props } = this.props;

    return isAuthenticated ?
      <App {...props}/> :
      <Redirect to={{ pathname: '/login', state: { from: props.location } }}/>;
  }
}

AuthRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
  path: PropTypes.string.isRequired,
  exact: PropTypes.bool,
};

const mapStateToProps = ({ auth }) => ({
  isAuthenticated: auth.isAuthenticated,
});

export default connect(mapStateToProps)(AuthRoute);
