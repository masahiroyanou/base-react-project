import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image, Card, Icon } from 'semantic-ui-react';

import './home.css';

class HomePage extends Component {

  render() {
    return (
      <div className="home-page">
        <h1>This is Home Page</h1>
        <Card>
          <Image src='https://react.semantic-ui.com/images/avatar/large/matthew.png'/>
          <Card.Content>
            <Card.Header>Matthew</Card.Header>
            <Card.Meta>
              <span className='date'>Joined in 2015</span>
            </Card.Meta>
            <Card.Description>Matthew is a musician living in Nashville.</Card.Description>
          </Card.Content>
          <Card.Content extra>
            <a>
              <Icon name='user'/>
              22 Friends
            </a>
          </Card.Content>
        </Card>
      </div>
    );
  }
}

export default connect(null, null)(HomePage);