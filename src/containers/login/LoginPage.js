import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Form, Input, Segment } from 'semantic-ui-react';

import { doLogin } from '../../actions/auth';
import Loading from '../../components/loading/Loading';
import './login.css';

class LoginPage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      errors: '',
    };
  }

  onClickLogin = () => {
    const email = this.state.email.trim();
    const password = this.state.password.trim();

    const errors = {};
    if (!email) {
      errors.email = 'Please input Email';
    }
    if (!password) {
      errors.password = 'Please input Password';
    }

    this.setState({ errors });
    if (Object.keys(errors).length) {
      return;
    }

    const { dispatch } = this.props;
    dispatch(doLogin(email, password));
  };

  render() {
    const { isOperating, message } = this.props;
    const { email, password, errors } = this.state;

    return (
      <div className="login-page text-center">
        <Segment className="form-area" padded>
          <Form>
            {message && <div className="text-danger">{message}</div>}
            <Form.Field>
              <label>Emil</label>
              {errors.email && <div className="text-danger">{errors.email}</div>}
              <Input
                type="text" placeholder="Emil" value={email}
                onChange={e => this.setState({ email: e.target.value })}
              />
            </Form.Field>
            <Form.Field>
              <label>Password</label>
              {errors.password && <div className="text-danger">{errors.password}</div>}
              <Input
                type="password" placeholder="password" value={password}
                onChange={e => this.setState({ password: e.target.value })}
              />
            </Form.Field>
            <Form.Field>
              <Button color="teal" fluid type="submit" onClick={this.onClickLogin}>Login</Button>
              {isOperating && <Loading message="Logining ......" inverted/>}
            </Form.Field>
          </Form>
        </Segment>
      </div>
    );
  }
}

const mapStateToProps = ({ auth }) => ({
  isOperating: auth.isOperating,
  message: auth.message,
});

export default connect(mapStateToProps)(LoginPage);
