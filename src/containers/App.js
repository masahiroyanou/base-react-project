import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getMe } from '../actions/member';
import Loading from '../components/loading/Loading';

class App extends Component {

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(getMe());
  }

  render() {
    const { isLoaded, component: Component, ...props } = this.props;
    if (!isLoaded) {
      return <Loading inverted/>;
    }

    return <Component {...props}/>;
  }
}

App.propTypes = {
  component: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
};

const mapStateToProps = ({ auth, member }) => ({
  isLoaded: !!member.members[auth.userId],
  own: member.members[auth.userId],
});

export default connect(mapStateToProps)(App);
