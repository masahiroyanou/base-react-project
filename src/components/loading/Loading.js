import React from 'react';
import PropTypes from 'prop-types';
import { Dimmer, Loader } from 'semantic-ui-react';
import classnames from 'classnames';

import './loading.css';

const Loading = ({ message, inverted, fullscreen }) => (
  <div className={classnames({ 'loading-not-fullscreen': !fullscreen })}>
    <Dimmer active inverted={inverted}>
      <Loader inverted={inverted} content={message}/>
    </Dimmer>
  </div>
);

Loading.propTypes = {
  message: PropTypes.string,
  inverted: PropTypes.bool,
  fullscreen: PropTypes.bool,
};

Loading.defaultProps = {
  message: 'Loading ......',
  inverted: false,
  fullscreen: true,
};

export default Loading;