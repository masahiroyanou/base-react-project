import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import queryString from 'query-string';

class IndexPage extends Component {

  componentWillMount() {
    const { location, dispatch } = this.props;

    const queries = queryString.parse(location.search);
    if (queries.path && queries.path !== '') {
      const otherQueries = Object.keys(queries).filter(key => key !== 'path').map(key => `${key}=${queries[key]}`).join('&');
      dispatch(push(`/${queries.path}${otherQueries ? `&${otherQueries}` : ''}`));
    } else {
      dispatch(push('/home'));
    }
  }

  render() {
    return <div/>;
  }
}

export default connect()(IndexPage);
