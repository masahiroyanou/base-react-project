import Api from './api';
import { User } from '../object/object';

export const REQ_LOADING_MEMBER = 'req_loading_member';
export const SUCCESS_GET_MEMBERS = 'success_get_members';
export const ERROR_LOADING_MEMBERS = 'error_loading_members';

const getMe = () => (dispatch, getState) => {
  if (getState().member.isLoading) {
    return;
  }

  dispatch({ type: REQ_LOADING_MEMBER });

  Api.client
    .get('/me')
    .then(response => response.data)
    .then(json => dispatch({ type: SUCCESS_GET_MEMBERS, members: [User.parse(json)] }))
    .catch(() => dispatch({ type: ERROR_LOADING_MEMBERS }));
};

export { getMe };
