import { push } from 'react-router-redux';

import Api from './api';

export const REQ_LOGIN = 'req_login';
export const MOVE_HOME = 'move_home';
export const ERROR_LOGIN = 'error_login';

export const LOGOUT = 'logout';

const doLogin = (email, password) => dispatch => {
  dispatch({ type: REQ_LOGIN });

  Api.client
    .post('/login', { email, password })
    .then(({ data }) => {
      const { token, userId } = data;

      Api.setToken(token);
      dispatch({ type: MOVE_HOME, token: token, userId });
      dispatch(push('/home'));
    })
    .catch(e => {
      let message = 'Happen System Error. Please try again later';
      if (e.response && e.response.status === 401) {
        message = 'Do not match email and password';
      }
      dispatch({ type: ERROR_LOGIN, message });
    });
};

const skipLogin = (token, path = '/home') => dispatch => {
  const { userId } = JSON.parse(decodeURIComponent(atob(token.split('.')[1])));

  Api.setToken(token);
  dispatch({ type: MOVE_HOME, token, userId });
  dispatch(push(path));
};

const logout = () => dispatch => {
  Api.removeToken();
  dispatch({ type: LOGOUT });
  dispatch(push('/login'));
};

export { doLogin, skipLogin, logout };
