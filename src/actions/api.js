import axios from 'axios';

import { logout } from './auth';

class Api {

  constructor() {
    this._client = axios.create({ baseURL: '/api', timeout: 60000 });
    this._client.interceptors.request.use(config => {
      config.headers['X-Request-ID'] = new Date().getTime();
      return config;
    }, error => Promise.reject(error));
  }

  get client() {
    return this._client;
  }

  setTokenOnly(token) {
    this._client.defaults.headers.common.Authorization = `Bearer ${token}`;
  }

  setToken(token) {
    this.setTokenOnly(token);

    const _this = this;
    this._interceptor = this._client.interceptors.response.use(
      response => response,
      error => {
        if (error.response) {
          const { status } = error.response;
          if (status === 401) {
            _this._store.dispatch(logout());
          } else if (500 <= status) {
            // TODO should consider
          }
        }
        return Promise.reject(error);
      },
    );
  }

  removeToken() {
    delete this._client.defaults.headers.common['Authorization'];

    this._client.interceptors.request.eject(this._interceptor);
    this._interceptor = null;
  }

}

export default new Api();
