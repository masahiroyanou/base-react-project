import objectAssign from 'object-assign';

import { ERROR_LOGIN, MOVE_HOME, REQ_LOGIN, } from '../actions/auth';

const INIT_STATE = {
  isOperating: false,
  isAuthenticated: false,
  token: '',
  userId: '',
  message: '',
};

const auth = (state = INIT_STATE, action) => {
  switch (action.type) {
    case REQ_LOGIN:
      localStorage.removeItem('token');
      return objectAssign({}, INIT_STATE, { isOperating: true });
    case MOVE_HOME:
      localStorage.setItem('token', action.token);
      return objectAssign({}, state, {
        isOperating: false,
        isAuthenticated: true,
        token: action.token,
        userId: action.userId,
        message: '',
      });
    case ERROR_LOGIN:
      return objectAssign({}, INIT_STATE, { message: action.message });
    default:
      return state;
  }
};

export default auth;
