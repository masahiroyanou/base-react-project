import objectAssign from 'object-assign';
import { ERROR_LOADING_MEMBERS, REQ_LOADING_MEMBER, SUCCESS_GET_MEMBERS, } from '../actions/member';

const INIT_STATE = {
  isLoading: false,
  members: {},
};

const member = (state = INIT_STATE, action) => {
  switch (action.type) {
    case REQ_LOADING_MEMBER:
      return objectAssign({}, state, { isLoading: true });
    case SUCCESS_GET_MEMBERS: {
      const members = objectAssign({}, state.members);
      action.members.forEach(member => members[member.id] = member);
      return objectAssign({}, state, {
        isLoading: false,
        members,
      });
    }
    case ERROR_LOADING_MEMBERS:
      return objectAssign({}, state, { isLoading: false });
    default:
      return state;
  }
};

export default member;
