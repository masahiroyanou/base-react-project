import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { Route, Switch } from 'react-router-dom';
import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { ConnectedRouter, routerMiddleware, routerReducer } from 'react-router-redux';
import createHistory from 'history/createHashHistory';
import { createLogger } from 'redux-logger';

import 'semantic-ui-css/semantic.min.css';
import './index.css';

import Api from './actions/api';
import reducer from './reducers/reducer';

import AuthRoute from './AuthRoute';
import LoginPage from './containers/login/LoginPage';
import IndexPage from './IndexPage';
import HomePage from './containers/home/HomePage';

import registerServiceWorker from './registerServiceWorker';

const history = createHistory();
const middlewares = [routerMiddleware(history), thunk];

let store;
if (process.env.NODE_ENV === 'production') {
  store = createStore(
    combineReducers({ ...reducer, routing: routerReducer }),
    applyMiddleware(...middlewares)
  );
} else {
  // react-logger
  middlewares.push(createLogger({ duration: true }));

  // redux-devtools-extension
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  store = createStore(
    combineReducers({ ...reducer, routing: routerReducer }),
    composeEnhancers(applyMiddleware(...middlewares))
  );
}

Api._store = store;

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Switch>
        <Route path='/login' component={LoginPage}/>
        <AuthRoute exact path="/" component={IndexPage}/>
        <AuthRoute exact path='/home' component={HomePage}/>
        <Route><h1>Not Found</h1></Route>
      </Switch>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
